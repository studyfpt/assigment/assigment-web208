import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import {
  BrowserModule,
  provideClientHydration,
} from '@angular/platform-browser';
import { ToastModule } from 'primeng/toast';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ProductAddComponent } from './layout/admin/product/product-add/product-add.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NavbarComponent } from './layout/admin/components/navbar/navbar.component';
import { SidebarComponent } from './layout/admin/components/sidebar/sidebar.component';
import { ClientComponent } from './layout/client/client.component';
import { AdminComponent } from './layout/admin/admin.component';
import { HttpClientModule } from '@angular/common/http';
import { RegisterComponent } from './layout/client/register/register.component';
import { LoginComponent } from './layout/client/login/login.component';
import { BannerComponent } from './layout/client/components/banner/banner.component';
import { FooterComponent } from './layout/client/components/footer/footer.component';
import { HeaderBarComponent } from './layout/client/components/header-bar/header-bar.component';
import { HeaderComponent } from './layout/client/components/header/header.component';
import { NavbarClientComponent } from './layout/client/components/navbar/navbar.component';
import { ProductsComponent } from './layout/client/products/products.component';
import { CategorysComponent } from './layout/client/products/components/categorys/categorys.component';
import { RelatedComponent } from './layout/client/product-detail/components/related/related.component';
import { ReviewsComponent } from './layout/client/product-detail/components/reviews/reviews.component';
import { DetailComponent } from './layout/client/product-detail/components/detail/detail.component';
import { CartComponent } from './layout/client/cart/cart.component';
import { CartListComponent } from './layout/client/cart/components/cart-list/cart-list.component';
import { CartTotalComponent } from './layout/client/cart/components/cart-total/cart-total.component';
import { LatestComponent } from './layout/client/home/components/latest/latest.component';
import { PopularProductsComponent } from './layout/client/home/components/popular-products/popular-products.component';
import { SliderComponent } from './layout/client/home/components/slider/slider.component';
import { HomeComponent } from './layout/client/home/home.component';
import { ProductDetailComponent } from './layout/client/product-detail/product-detail.component';
import { ProductListClientComponent } from './layout/client/products/components/product-list/product-list.component';
import { ProductClientComponent } from './layout/client/components/product/product.component';
import { StatisticalComponent } from './layout/admin/statistical/statistical.component';
import { ProductListComponent } from './layout/admin/product/product-list/product-list.component';
import { ProductEditComponent } from './layout/admin/product/product-edit/product-edit.component';
import { CategoryAddComponent } from './layout/admin/category/category-add/category-add.component';
import { CategoryListComponent } from './layout/admin/category/category-list/category-list.component';
import { CategoryEditComponent } from './layout/admin/category/category-edit/category-edit.component';
import { CommonModule } from '@angular/common';
import { MessageService } from 'primeng/api';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
@NgModule({
  declarations: [
    AppComponent,
    ProductAddComponent,
    NavbarComponent,
    SidebarComponent,
    ClientComponent,
    AdminComponent,
    RegisterComponent,
    LoginComponent,
    BannerComponent,
    FooterComponent,
    HeaderBarComponent,
    HeaderComponent,
    NavbarClientComponent,
    ProductsComponent,
    CategorysComponent,
    RelatedComponent,
    ReviewsComponent,
    DetailComponent,
    CartComponent,
    CartListComponent,
    CartTotalComponent,
    LatestComponent,
    PopularProductsComponent,
    SliderComponent,
    HomeComponent,
    ProductDetailComponent,
    ProductListClientComponent,
    ProductClientComponent,
    StatisticalComponent,
    ProductListComponent,
    ProductEditComponent,
    CategoryAddComponent,
    CategoryListComponent,
    CategoryEditComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    ToastModule,
    CommonModule,
  ],
  providers: [provideClientHydration(), MessageService],
  bootstrap: [AppComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
})
export class AppModule {}
