import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ClientComponent } from './layout/client/client.component';
import { AdminComponent } from './layout/admin/admin.component';
import { ProductAddComponent } from './layout/admin/product/product-add/product-add.component';
import { RegisterComponent } from './layout/client/register/register.component';
import { LoginComponent } from './layout/client/login/login.component';
import { HomeComponent } from './layout/client/home/home.component';
import { CartComponent } from './layout/client/cart/cart.component';
import { ProductsComponent } from './layout/client/products/products.component';
import { ProductDetailComponent } from './layout/client/product-detail/product-detail.component';
import { StatisticalComponent } from './layout/admin/statistical/statistical.component';
import { ProductListComponent } from './layout/admin/product/product-list/product-list.component';
import { ProductEditComponent } from './layout/admin/product/product-edit/product-edit.component';
import { CategoryAddComponent } from './layout/admin/category/category-add/category-add.component';
import { CategoryListComponent } from './layout/admin/category/category-list/category-list.component';
import { CategoryEditComponent } from './layout/admin/category/category-edit/category-edit.component';
import { adminGuard } from './Guard/admin.guard';

const routes: Routes = [
  {
    path: '',
    component: ClientComponent,
    children: [
      { path: '', component: HomeComponent },
      { path: 'cart', component: CartComponent },
      { path: 'shop', component: ProductsComponent },
      { path: 'product-detail/:id', component: ProductDetailComponent },
      { path: 'register', component: RegisterComponent },
      { path: 'login', component: LoginComponent },
    ],
  },
  {
    path: 'admin',
    component: AdminComponent,
    children: [
      { path: '', component: StatisticalComponent },
      {
        path: 'product',
        children: [
          { path: 'add', component: ProductAddComponent },
          { path: 'list', component: ProductListComponent },
          { path: 'edit/:id', component: ProductEditComponent },
        ],
      },
      {
        path: 'category',
        children: [
          { path: 'add', component: CategoryAddComponent },
          { path: 'list', component: CategoryListComponent },
          { path: 'edit/:id', component: CategoryEditComponent },
        ],
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
