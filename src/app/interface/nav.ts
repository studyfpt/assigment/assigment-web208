export interface INav {
  id: number,
  name: string,
  path: string,
  icon: string,
  parent : number
  }
