export interface IProduct {
  id?: number;
  name: string;
  thumnail: string;
  image1: string;
  image2: string;
  description: string;
  price: string;
  stock: number;
  category: number;
}
