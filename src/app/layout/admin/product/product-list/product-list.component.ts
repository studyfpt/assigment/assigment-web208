import { Component, OnInit } from '@angular/core';
import { IProduct } from '../../../../interface/product';
import { ProductService } from '../../../../service/product.service';
import { CategoryService } from '../../../../service/category.service';
import { ICategory } from '../../../../interface/category';
@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.css'],
})
export class ProductListComponent implements OnInit {
  products: IProduct[] = [];
  loading = true;
  Categories: ICategory[] = [];

  constructor(
    private productService: ProductService,
    private categoryService: CategoryService
  ) {}

  ngOnInit() {
    this.productService.getAll().subscribe((data: IProduct[]) => {
      this.products = data;
    
      console.log(this.products);
      this.loading = false;
    });

    this.categoryService.getAll().subscribe((categories: ICategory[]) => {
      console.log(categories);

      this.Categories = categories;
    });
  }

  getCategoryName(categoryId: number): string {
    const category = this.Categories.find((cat) => cat.id == categoryId);
    return category ? category.name : 'Unknown';
  }


  onDelete(id?: number) {
    if (confirm('Are you sure you want to delete this product?') && id) {
      this.productService.delete(id).subscribe(() => {
        this.products = this.products.filter((product) => product.id !== id);
        alert('Delete Successfully');
      });
    }
  }
}
