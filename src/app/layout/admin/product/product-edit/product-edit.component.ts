import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { IProduct } from '../../../../interface/product';
import { ProductService } from '../../../../service/product.service';
import { CategoryService } from './../../../../service/category.service';
import { ICategory } from '../../../../interface/category';

@Component({
  selector: 'app-product-edit',
  templateUrl: './product-edit.component.html',
  styleUrls: ['./product-edit.component.css'],
})
export class ProductEditComponent implements OnInit {
  productForm: FormGroup;
  Categories: ICategory[] = [];
  productId: number;
  submitted = false;

  constructor(
    private productService: ProductService,
    private categoryService: CategoryService,
    private route: ActivatedRoute,
    private router: Router
  ) {
    this.productId = +this.route.snapshot.paramMap.get('id')!;
    this.productForm = new FormGroup({
      name: new FormControl('', [Validators.required, Validators.minLength(6)]),
      thumnail: new FormControl('', [Validators.required]),
      image1: new FormControl('', [Validators.required]),
      image2: new FormControl('', [Validators.required]),
      description: new FormControl('', [Validators.required]),
      price: new FormControl('', [Validators.required]),
      stock: new FormControl(0, [Validators.required, Validators.min(0)]),
      category: new FormControl('', [Validators.required, Validators.min(1)]),
    });
  }

  ngOnInit() {
    this.categoryService.getAll().subscribe((categories: ICategory[]) => {
      this.Categories = categories;
    });

    this.productService
      .getById(this.productId)
      .subscribe((product: IProduct) => {
        this.productForm.setValue({
          name: product.name,
          thumnail: product.thumnail,
          image1: product.image1,
          image2: product.image2,
          description: product.description,
          price: product.price,
          stock: product.stock,
          category: product.category,
        });
      });
  }

  onSubmit() {
    this.submitted = true;
    console.log(this.productForm.value);

    if (this.productForm.valid) {
      this.productService
        .edit(this.productId, this.productForm.value as IProduct)
        .subscribe((data: IProduct) => {
          console.log(data);
          alert('Product updated successfully!');
          this.router.navigate(['/admin/product/list']);
        });
    }
  }
}
