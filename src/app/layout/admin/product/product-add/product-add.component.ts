import { Component } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { IProduct } from '../../../../interface/product';
import { ProductService } from '../../../../service/product.service';
import { CategoryService } from './../../../../service/category.service';
import { ICategory } from '../../../../interface/category';
import { Router } from '@angular/router';

@Component({
  selector: 'app-product-add',
  templateUrl: './product-add.component.html',
  styleUrls: ['./product-add.component.css'],
})
export class ProductAddComponent {
  constructor(
    private ProductService: ProductService,
    private CategoryService: CategoryService,
    private router: Router
  ) {}
  Categories: ICategory[] = [];
  submitted = false;
  productForm = new FormGroup({
    name: new FormControl('', [Validators.required, Validators.minLength(6)]),
    thumnail: new FormControl('https://picsum.photos/200/300?random=3', [
      Validators.required,
    ]),
    image1: new FormControl('https://picsum.photos/200/300?random=1', [
      Validators.required,
    ]),
    image2: new FormControl('https://picsum.photos/200/300?random=2', [
      Validators.required,
    ]),
    description: new FormControl('', [Validators.required]),
    price: new FormControl('', [Validators.required]),
    stock: new FormControl(0, [Validators.required, Validators.min(0)]),
    category: new FormControl(0, [Validators.required, Validators.min(1)]),
  });

  ngOnInit() {
    this.CategoryService.getAll().subscribe((categories: ICategory[]) => {
      this.Categories = categories;
      if (this.Categories.length > 0) {
        this.productForm.get('category')?.setValue(this.Categories[0]?.id ?? 0);
      }
    });
  }

  onSubmit() {
    this.submitted = true;
    console.log(this.productForm.value);

    if (this.productForm.valid) {
      this.ProductService.add(this.productForm.value as IProduct).subscribe(
        (data: IProduct) => {
          console.log(data);
          alert('Product added successfully!');
          this.router.navigate(['/admin/product/list']);
        }
      );
    }
  }
}
