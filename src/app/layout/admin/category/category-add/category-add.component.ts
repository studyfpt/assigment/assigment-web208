import { Component } from '@angular/core';
import { CategoryService } from '../../../../service/category.service';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ICategory } from '../../../../interface/category';

@Component({
  selector: 'app-category-add',
  templateUrl: './category-add.component.html',
  styleUrls: ['./category-add.component.css'], // Corrected here
})
export class CategoryAddComponent {
  constructor(private categoryService: CategoryService) {}

  submited: boolean = false;

  formCategory = new FormGroup({
    name: new FormControl('', [Validators.required, Validators.minLength(3)]),
  });

  onSubmit() {
    this.submited = true; // Set submitted to true to show validation errors
    if (this.formCategory.valid) {
      this.categoryService.add(this.formCategory.value as ICategory).subscribe(
        () => {
          alert('Add category successfully!');
          this.formCategory.reset();
          this.submited = false;
        },
        (error) => {
          console.log(error);
        }
      );
    }
  }
}
