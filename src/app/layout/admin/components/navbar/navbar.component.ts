import { Component, Output, EventEmitter } from '@angular/core';
import { INav } from '../../../../interface/nav';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css'],
})
export class NavbarComponent {
  @Output() toggleTheme = new EventEmitter<void>();

  navs: INav[] = [
    {
      id: 1,
      name: 'Thống Kê',
      path: '/admin',
      icon: 'icon _page.svg',
      parent: 0,
    },
    {
      id: 2,
      name: 'Quản Lý Loại Sản Phẩm',
      path: 'category',
      icon: 'icon _cart.svg',
      parent: 0,
    },
    {
      id: 3,
      name: 'Quản Lý Sản Phẩm',
      path: 'product',
      icon: 'icon _box.svg',
      parent: 0,
    },
    {
      id: 4,
      name: 'Quản Lý Tài Khoản',
      path: 'account',
      icon: 'icon _user.svg',
      parent: 0,
    },
    {
      id: 5,
      name: 'Thêm Sản Phẩm',
      path: 'add',
      icon: 'icon _page.svg',
      parent: 3,
    },
    {
      id: 6,
      name: 'Danh sách Sản Phẩm',
      path: 'list',
      icon: 'icon _page.svg',
      parent: 3,
    },
    {
      id: 7,
      name: 'Danh sách Loại',
      path: 'list',
      icon: 'icon _page.svg',
      parent: 2,
    },
    {
      id: 8,
      name: 'Thêm Loại',
      path: 'add',
      icon: 'icon _page.svg',
      parent: 2,
    },
  ];

  item: number | null = null;

  toggleItem(id: number) {
    this.item = this.item === id ? null : id;
  }
  hasChildMenus(nav: any): boolean {
    return this.navs.some((item) => item.parent === nav.id);
  }
  isAtEnd = false;

  toggleSidebar(): void {
    this.isAtEnd = !this.isAtEnd;
    this.toggleTheme.emit();
  }
}
