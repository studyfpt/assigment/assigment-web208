import { Component } from '@angular/core';
import axios from 'axios';
import { IProduct } from '../../../../../interface/product';
import { ProductService } from '../../../../../service/product.service';

@Component({
  selector: 'app-popular-products',
  templateUrl: './popular-products.component.html',
  styleUrl: './popular-products.component.css',
})
export class PopularProductsComponent {
  products: IProduct[] = [];

  constructor(private productService: ProductService) {}

  ngOnInit() {
    this.productService
      .getAll()
      .subscribe((data: IProduct[]) => (this.products = data.slice(0, 8)));
  }
}
