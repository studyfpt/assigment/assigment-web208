import { Component } from '@angular/core';
import { IProduct } from '../../../../../interface/product';
import { ProductService } from '../../../../../service/product.service';

@Component({
  selector: 'app-related',
  templateUrl: './related.component.html',
  styleUrl: './related.component.css',
})
export class RelatedComponent {
  products: IProduct[] = [];

  constructor(private productService: ProductService) {}

  ngOnInit() {
    this.productService
      .getAll()
      .subscribe((data: IProduct[]) => (this.products = data.slice(0, 4)));
  }
}
