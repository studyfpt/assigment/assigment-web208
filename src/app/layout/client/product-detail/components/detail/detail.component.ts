import { Component, Input } from '@angular/core';
import { IProduct } from '../../../../../interface/product';
import { ActivatedRoute } from '@angular/router';
import { ProductService } from '../../../../../service/product.service';
import { CategoryService } from '../../../../../service/category.service';
import { ICategory } from '../../../../../interface/category';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrl: './detail.component.css',
})
export class DetailComponent {
  constructor(
    private router: ActivatedRoute,
    private productService: ProductService,
    private categoryService: CategoryService
  ) {}
  imageMain: string = '';
  product: IProduct = {} as IProduct;
  productId = this.router.snapshot.paramMap.get('id');
  Categories: ICategory[] = [];
  ngOnInit() {
    console.log(this.productId);
    if (!this.productId) {
      window.location.replace('/');
    } else {
      this.productService
        .getById(Number(this.productId))
        .subscribe((data: any) => {
          this.product = data;
          this.imageMain = this.product.thumnail;
        });
    }
    this.categoryService.getAll().subscribe((categories: ICategory[]) => {
      console.log(categories);
      this.Categories = categories;
    });
  }
  getCategoryName(categoryId: number): string {
    const category = this.Categories.find((cat) => cat.id == categoryId);
    return category ? category.name : 'Unknown';
  }
  onChangeImage(image: string) {
    this.imageMain = image;
  }
}
