import { Component, EventEmitter, Input, Output } from '@angular/core';
import { ICategory } from '../../../../../interface/category';

@Component({
  selector: 'app-categorys',
  templateUrl: './categorys.component.html',
  styleUrl: './categorys.component.css',
})
export class CategorysComponent {
  @Input() categorys: ICategory[] = [];
  @Output() categoryChange = new EventEmitter<string>();
  @Input() totalProductCount = new EventEmitter<number>();
  @Output() searchQueryChange = new EventEmitter<string>();

  onCategoryChange(category: string) {
    this.categoryChange.emit(category);
  }

  onSearchQueryChange(event: Event) {
    const inputElement = event.target as HTMLInputElement;
    this.searchQueryChange.emit(inputElement.value);
  }
  removeKey() {
    window.location.href = '/shop';
  }
  changeKey(key: string) {
    window.location.href = '/shop?category=' + key;
  }
}
