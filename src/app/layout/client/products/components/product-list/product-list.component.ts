import { Component, Input } from '@angular/core';
import { IProduct } from '../../../../../interface/product';

@Component({
  selector: 'app-product-list-client',
  templateUrl: './product-list.component.html',
  styleUrl: './product-list.component.css',
})
export class ProductListClientComponent {
  @Input() products: IProduct[] = [];
}
