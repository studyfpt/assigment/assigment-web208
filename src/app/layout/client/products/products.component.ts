import { Component, OnInit } from '@angular/core';
import { CategoryService } from './../../../service/category.service';
import { ICategory } from '../../../interface/category';
import { IProduct } from '../../../interface/product';
import { ProductService } from '../../../service/product.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css'],
})
export class ProductsComponent implements OnInit {
  categorys: ICategory[] = [];
  products: IProduct[] = [];
  filteredProducts: IProduct[] = [];
  selectedCategory: string = '';
  searchQuery: string = '';
  totalCountProduct: number = 0;
  keyword: string = '';
  category: string = '';
  constructor(
    private categoryService: CategoryService,
    private productService: ProductService,
    private route: ActivatedRoute
  ) {}

  ngOnInit() {
    this.categoryService.getAll().subscribe((data: ICategory[]) => {
      this.categorys = data;
      console.log(this.categorys);
    });
    this.keyword = this.route.snapshot.queryParams['key'] || '';
    this.category = this.route.snapshot.queryParams['category'] || '';
    this.productService
      .getProductByName(this.keyword)
      .subscribe((data: IProduct[]) => {
        this.products = data;
        this.filteredProducts = data;
        this.productService
          .getProductByCategory(this.category)
          .subscribe((data: IProduct[]) => {
            this.filteredProducts = data;
          });
        console.log(this.filteredProducts);
        console.log(this.keyword);
        console.log(this.category);
        this.totalCountProduct = this.filteredProducts.length;
      });
  }

  onCategoryChange(category: string) {
    this.selectedCategory = category;
    this.filterProducts();
  }
  onSearchQueryChange(query: string) {
    this.searchQuery = query;
    this.filterProducts();
  }

  filterProducts() {
    this.filteredProducts = this.products.filter((product) => {
      const matchesCategory = this.selectedCategory
        ? Number(product.category) == Number(this.selectedCategory)
        : true;
      const matchesSearchQuery = this.searchQuery
        ? product.name.toLowerCase().includes(this.searchQuery.toLowerCase())
        : true;
      return matchesCategory && matchesSearchQuery;
    });
  }
}
