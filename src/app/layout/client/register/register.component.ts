import { Component } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { AuthService } from './../../../service/auth.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrl: './register.component.css'
})
export class RegisterComponent {
  constructor(private AuthService: AuthService) {}

  submitted = false;

  authForm = new FormGroup({
    name: new FormControl('', [Validators.required, Validators.minLength(6)]),
    username: new FormControl('', [Validators.required, Validators.minLength(6)]),
    email: new FormControl('', [Validators.required, Validators.email]),
    password: new FormControl('', [Validators.required, Validators.minLength(9)]),
  });
  onSubmit() {
    console.log(this.authForm.value);

    this.submitted = true;
    if (this.authForm.valid) {
      this.AuthService.register(this.authForm.value as any).subscribe(
        (data) => {
          console.log(data);
          alert('Register successfully!');
          window.location.replace('/login');
        }
      );
    }
  }
}
