import { Component, Input } from '@angular/core';
import { MessageService } from 'primeng/api';

@Component({
  selector: 'app-product-client',
  templateUrl: './product.component.html',
  styleUrl: './product.component.css',
})
export class ProductClientComponent {
  constructor(private messageService: MessageService) {}
  @Input() product: any;
  buttonLabel: string = 'Add to cart';
  show() {
    this.messageService.add({
      severity: 'success',
      summary: 'Success',
      detail: 'Add to cart successfully',
    });
  }
}
