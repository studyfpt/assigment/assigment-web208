import { Component } from '@angular/core';
import { IProduct } from '../../../../interface/product';
import { FormControl, FormGroup } from '@angular/forms';
import { ProductService } from '../../../../service/product.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrl: './header.component.css',
})
export class HeaderComponent {
  constructor(private productService: ProductService) {}
  products: IProduct[] = [];

  formSearch = new FormGroup({
    search: new FormControl(''),
  });
  onSubmit = () => {
    console.log(this.formSearch.value);
    window.location.href = '/shop?key=' + this.formSearch.value.search;
  };
}
