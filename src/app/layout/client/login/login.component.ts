import { Component } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { AuthService } from '../../../service/auth.service';
import { jwtDecode } from 'jwt-decode';
import { MessageService } from 'primeng/api';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
  providers: [MessageService],
})
export class LoginComponent {
  constructor(
    private messageService: MessageService,
    private AuthService: AuthService,
    private router: Router
  ) {}

  show() {
    return new Promise<void>((resolve) => {
      this.messageService.add({
        severity: 'success',
        summary: 'Success',
        detail: 'Login successfully',
        life: 1000,
      });
      setTimeout(() => {
        resolve();
      }, 1000);
    });
  }

  time = Date.now() / 1000;
  ngOnInit() {
    if (this.AuthService.checkUser()) {
      this.router.navigate(['/admin']);
    }
  }

  submitted = false;

  authForm = new FormGroup({
    email: new FormControl('', [Validators.required, Validators.email]),
    password: new FormControl('', [
      Validators.required,
      Validators.minLength(9),
    ]),
  });

  onSubmit() {
    console.log(this.authForm.value);

    this.submitted = true;
    if (this.authForm.valid) {
      this.AuthService.login(this.authForm.value as any).subscribe(
        async (data) => {
          console.log(data);
          localStorage.setItem('user', data?.accessToken);
          await this.show();
          this.router.navigate(['/admin']).then((success) => {
            if (success) {
              console.log('Navigation to /admin successful!');
            } else {
              console.error('Navigation to /admin failed!');
            }
          });
        },
        (error) => {
          alert('Login failed!');
        }
      );
    }
  }
}
