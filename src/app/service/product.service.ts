import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { IProduct } from '../interface/product';

@Injectable({
  providedIn: 'root',
})
export class ProductService {
  constructor(private http: HttpClient) {}
  API_URL: string = 'http://localhost:3000/products';

  getAll = (): Observable<any> => {
    return this.http.get(this.API_URL);
  };
  getProductByName = (name: string): Observable<any> => {
    return this.http.get(this.API_URL + '?name_like=' + name);
  };
  getProductByCategory = (id: string): Observable<any> => {
    return this.http.get(this.API_URL + '?category_like=' + id);
  };
  getById = (id: number): Observable<any> => {
    return this.http.get(this.API_URL + '/' + id);
  };
  add = (data: IProduct): Observable<any> => {
    return this.http.post(this.API_URL, data);
  };
  edit = (id: number, data: IProduct): Observable<any> => {
    return this.http.put(this.API_URL + '/' + id, data);
  };
  delete = (id: number): Observable<any> => {
    return this.http.delete(this.API_URL + '/' + id);
  };
}
