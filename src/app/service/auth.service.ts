import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { jwtDecode } from 'jwt-decode';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  constructor(private http: HttpClient) {}
  API_URL: string = 'http://localhost:3000/';

  register = (data: any): Observable<any> => {
    return this.http.post(this.API_URL + 'users', data);
  };
  login = (data: any): Observable<any> => {
    return this.http.post(this.API_URL + 'login', data);
  };
  checkUser = (): boolean => {
    let check: boolean = false;
    const token = localStorage.getItem('user');
    try {
      const decode: any = jwtDecode(token as string);
      if (decode.exp > Date.now() / 1000 && decode.sub == 1) {
        check = true;
      }
    } catch (error) {
      console.log(error);
    }
    return check;
  };
}
