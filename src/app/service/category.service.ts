import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ICategory } from '../interface/category';

@Injectable({
  providedIn: 'root',
})
export class CategoryService {
  constructor(private http: HttpClient) {}
  API_URL: string = 'http://localhost:3000/categories';

  getAll = (): Observable<any> => {
    return this.http.get(this.API_URL);
  };
  getById = (id: number): Observable<any> => {
    return this.http.get(this.API_URL + '/' + id);
  };
  add = (data: ICategory): Observable<any> => {
    console.log(data);

    return this.http.post(this.API_URL, data);
  };
  edit = (id: number, data: ICategory): Observable<any> => {
    return this.http.put(this.API_URL + '/' + id, data);
  };
  delete = (id: number): Observable<any> => {
    return this.http.delete(this.API_URL + '/' + id);
  };
}
