import { inject } from '@angular/core';
import { CanActivateFn } from '@angular/router';
import { AuthService } from '../service/auth.service';

export const adminGuard: CanActivateFn = (route, state) => {
  const userservice = inject(AuthService);
  if (userservice.checkUser()) {
    return true;
  } else {
    window.location.replace('/login');
    return false;
  }
};
